var colorList = ["#4EA8DE", "#72EFDD", "#64DFDF", "#56CFE1", "#48BFE3","#4EA8DE","#5390D9","#5E60CE","#6930C3","#7400B8"] ;


 let timer;



class CircleClass {
  constructor(px, py, s) {
    this.positionX = px;
    this.positionY = py;
    this.size = s;
    this.c = colorList[int(random(0, colorList.length))]
  }

  display() {
    circle(this.positionX, this.positionY, this.size);
    
    if (this.size > 15) {
      noStroke();
      fill(this.c);
    } else {
      noFill();
      strokeWeight(1);
      stroke(this.c);
    }
  }

  setColor(listOfColors){
    this.c = listOfColors[int(random(0, listOfColors.length))]
  }
}
//10-30
class CircleGrid {
  constructor() {
    this.gridSize = 30
    this.circles = [];
    this.radius=35;
    this.listOfColors = ["#4EA8DE", "#72EFDD", "#64DFDF", "#56CFE1", "#48BFE3","#4EA8DE","#5390D9","#5E60CE","#6930C3","#7400B8"];
  
    
    // console.log(this.gridSlider)

    for (let y = 0; y < video.height; y += this.gridSize) {
      var row = [];
      for (let x = 0; x < video.width; x += this.gridSize) {
        let index = (y * video.width + x) * 4;
        let r = video.pixels[index];
        let dia = map(r, 0, 255, this.gridSize, 2);
        row.push(
          new CircleClass(x + this.gridSize / 
                          2, y + this.gridSize / 2, dia)
        );
      }
      this.circles.push(row);
    }
  }

  display() {
    
    video.loadPixels();
    //this.gridSlider = slider.value()
    // this.gridSize = this.gridSlider
    
    //change circle size depending on brightness of pixel
    for (let i = 0; i < this.circles.length; i++) {
      for (let j = 0; j < this.circles[0].length; j++) {
        let index = (i * this.gridSize * video.width + 
                     j * this.gridSize) * 4;
        let r = video.pixels[index];
        let dia = map(r, 0, 255, this.radius, 0);
        this.circles[i][j].size = dia;
        this.circles[i][j].display();
      }
    }

    var selection1 = int(random(this.circles.length - 1));
    var selection2 = int(random(this.circles[0].length - 1));
    var col = this.listOfColors[int(random(0, this.listOfColors.length))];
    this.circles[selection1][selection2].c = col;
  }

  changeColor(listOfColors){
    this.listOfColors=listOfColors

    for (let i = 0; i < this.circles.length; i++) {
        for (let j = 0; j < this.circles[i].length; j++) {
          this.circles[i][j].setColor(this.listOfColors);
        }
      }
  
  }


  
}