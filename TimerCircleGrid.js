var listOfColors_red = ["#1c77c3", "#39a9db", "#40bcd8", "#f39237", "#d63230", "#540d6e", "#ee4266", "#ffd23f","#f3fcf0", "#19647E",];
var listOfColors_blue = ["#4EA8DE", "#72EFDD", "#64DFDF", "#56CFE1", "#48BFE3","#4EA8DE","#5390D9","#5E60CE","#6930C3","#7400B8"];
var pastelColors = ["#FFC3A0", "#FFAFBD", "#FFD3B5", "#FFD1DC", "#D6AEDD", "#C0E8E4", "#FFE8D6", "#E6B89C", "#E8C0AA", "#F2E9E4"];
var warmColors = ["#FF7F51", "#FF6B6B", "#FF9488", "#FFB997", "#FFDAC1", "#F9C22E", "#FFA41B", "#FFA400", "#FFB400", "#FFC100"];
var gradientColors = ["#F26B8C", "#F49D8A", "#F7CE89", "#F9FF87", "#C9DE93", "#FF9AA2", "#FFB7B2", "#FFD4C1", "#FFE2CF", "#FFF1DE"];


class TimerCircleGrid {
    constructor(totalTime,grid) {
      this.totalTime = totalTime;
      this.remainingTime = totalTime;
      this.repetition = totalTime;
      this.grid=grid
      this.ColorList = [];
      this.ColorList.push(listOfColors_red);
      this.ColorList.push(listOfColors_blue);
      this.ColorList.push(pastelColors);
      this.ColorList.push(warmColors);
      this.ColorList.push(gradientColors);


      
    }
  
    start() {
      this.timerInterval = setInterval(() => {
        if (this.remainingTime > 0) {
          this.remainingTime--;
        } else {
          this.setSeconds(this.repetition);
          this.grid.radius+=25
          this.grid.changeColor(this.ColorList[int(random(0,this.ColorList.length))]) 
          if(this.grid.radius>75){
            this.grid.radius =35                 
          }
            
         // current_mode++; 
         // if (current_mode > Object.keys(Modes).length) {
         //   current_mode = 0; 
         // }
          
        }
      }, 1000);
    }
  
    stop() {
      clearInterval(this.timerInterval);
    }
  
     display() {
       textSize(24);
       fill(0)
       textAlign(CENTER);
       text("Remaining Time: " + this.remainingTime, width / 2, height / 2);
     }

     setSeconds(seconds) {
        this.remainingTime = seconds;
        this.totalTime = seconds;
        this.repetition = seconds;
      }
  }